<?php include('header.php')?>

<div class="pagetop">
	<div class="container">
    	<div class="row">
        	<div class="col-md-6 col-sm-6">
            	<h2>Kathmandu Tour Package</h2>
            </div>
            <div class="col-md-6 col-sm-6">
            	<ol class="breadcrumb pull-right">
  <li><a href="index.php">Home</a></li>
  <li class="active">Kathmandu Tour Package</li>
</ol>
            </div>
        </div>
    </div>
</div>
  
  <div class="main page-wrapper">
    <div class="container">
      <div class="row">
        <div class="col-md-9 col-sm-9">
          
          
          
          
          <p>Kathmandu Valley - The Capital of NEPAL The word Kathmandu means a city of wooden arts. Kathmandu Valley, the capital and heart of the country essentially symbolises all that is Nepal. Having seen the rule of various dynasties, the culture and society of Kathmandu has evolved through time to give it a most unique characteristic. Kathmandu Valley, today is an urban city that has its history rooted in ancient myths yet stands testimonial to the greatness of the people who have lived here for centuries, a melting pot of all that is Nepali, a mystical magical kingdom where time passes by in a mist, a Shangri-La that took a while to get noticed by the rest of the world.</p>
                	
          
         
        </div>
        <div class="col-md-3 col-sm-3">
        	<div class="primary-section">
            	<ul>
					<li><a href="img/kathmandu.jpg" class="popup-img"><img src="img/kathmandu.jpg" alt=""></a></li>
                   
				</ul>
            </div>
        </div>
        
      </div>
      <hr/>
      <div class="row">
        <div class="col-md-9 col-sm-9">
          
          
          <div class="page-title">
                    	
                        <h3>Darbar Square</h3>
                    </div>
          
          <p>Kathmandu acquired its name from Kasthamandap, an open community hall allegedly built from the timber of one tree, on the western side of the Basantapur Durbar Square. Although its age is uncertain there is a plaque inside the structure that dates to 1048. Basantapur Durbar Square served as the main city square of ancient Kathmandu with the Hanuman Dhoka Palace, built Pratap Malla, one the greatest lovers of art to have ruled Kathmandu, as the residence of the royal families in the past. Named after the statue of the monkey god Hanuman kneeling on a pedestal in front of the main gate, the palace today is a museum. While the actual palace compound covers a large area, numerous other temples dedicated to various Hindu gods and goddesses such as the Taleju Bhawani, Kumari, Sweta Bhairav, Maru Ganesh, Akash Bhairav, Shiva Parvati etc surround the palace and are preserved as they were built hundreds of years ago. Visiting the Basantapur Durbar Square, especially during the festivals is like going back in time, as ancient traditions are carried out enthusiastically by the locals as they were done centuries ago.</p> <p>The Basantapur Durbar Square is one of the seven Monument Zones that make up the Kathmandu Valley UNESCO World Heritage Site. Perched on a hilltop on the south western part of Kathmandu, Swayambhunath, is one of the most important religious and cultural sites in Nepal and is associated with the birth of the Kathmandu Valley civilisation. When the Valley was still an ancient lake, a predecessor of the historic Siddartha Gautama Buddha planted the lotus seed here from which came the light of Swayambhu, the â€œSelf Bornâ€. The Boddisattva Manjushree drained the lake by cutting a gorge in the southern edge of the valley in a place called Chobahar from which the Bagmati river flows out, south bound, and established the Kathmandu Valley civilisation.</p>
                	
          
         
        </div>
        <div class="col-md-3 col-sm-3">
        	<div class="primary-section">
            	<ul>
					<li><a href="img/darbarSquare1.jpg" class="popup-img"><img src="img/darbarSquare1.jpg" alt=""></a></li>
                   	<li><a href="img/darbarSquare2.jpg" class="popup-img"><img src="img/darbarSquare2.jpg" alt=""></a></li>
                    <li><a href="img/darbarSquare3.jpg" class="popup-img"><img src="img/darbarSquare3.jpg" alt=""></a></li>
                  
				</ul>
            </div>
        </div>
        
      </div>
      <hr/>
      <div class="row">
        <div class="col-md-9 col-sm-9">
          
          
          <div class="page-title">
                    	
                        <h3>Swayambhunath</h3>
                    </div>
          
          <p>Approximately 2000 years old, Swayambhunath is perched on a hillock on the south-western edge of Kathmandu. The stupa, is a dome 20 meters in diameter and 32 meters high and is made of brick and earth mounted by a conical spire capped by a pinnacle of copper gilt. It is surrounded by many other smaller temples and places of religious importance for both Hindus and Buddhists, a perfect example of the symbiotic co-existence of different religious beliefs only found in Nepal. The main dome of Swayambhunath is surrounded by such Hindu and Buddhist temples like the Harati Mata temple, Shantipur, Anantapur and some Buddhist monasteries. The hill is heavily wooded on all sides with indigenous plant species, and troops of monkeys, giving it the name of Monkey Temple. The height of Swayambhunath also makes it a good vantage point and on clear sunny days one can see the Himalaya all the way to the east.</p> 
          
                	
          
         
        </div>
        <div class="col-md-3 col-sm-3">
        	<div class="primary-section">
            	<ul>
					<li><a href="img/syambhu3.jpg" class="popup-img"><img src="img/syambhu3.jpg" alt=""></a></li>
                   	<li><a href="img/syambhu4.jpg" class="popup-img"><img src="img/syambhu4.jpg" alt=""></a></li>
                   
                  
				</ul>
            </div>
        </div>
        
      </div>
      <hr/>
      <div class="row">
        <div class="col-md-9 col-sm-9">
          
          
          <div class="page-title">
                    	
                        <h3>Buddhanath</h3>
                    </div>
          
          <p>With a base of 82 meters in diameter, Boudhanath is claimed to be the largest Buddhist stupa in the world. There are many legends attached to Boudhanath chief among which is that of the 5th century Lichivi King Manadev who built it to do penance for parricide. Lost and forgotten for centuries Boudhanath was rediscovered in the 15th century from whence it slowly started gaining reputation among Tibetan Buddhists especially after the 19th century Rana Prime Minister appointed a Rana Prime Minister Jang Bahadur appointed a Tibetan monk as its chief abbot. Today there are more than 50 monasteries surrounding Boudhanath which is also one of the seven Monument Zone which make up the Kathmandu Valley World Heritage Site</p> 
          
                	
          
         
        </div>
        <div class="col-md-3 col-sm-3">
        	<div class="primary-section">
            	<ul>
					
                   	<li><a href="img/buddha3.jpg" class="popup-img"><img src="img/buddha3.jpg" alt=""></a></li>
                    <li><a href="img/buddha4.jpg" class="popup-img"><img src="img/buddha4.jpg" alt=""></a></li>
                   
                  
				</ul>
            </div>
        </div>
        
      </div>
       <hr/>
      <div class="row">
        <div class="col-md-9 col-sm-9">
          
          
          <div class="page-title">
                    	
                        <h3>Thamel</h3>
                    </div>
          
          <p>Thamel area has recently emerged as the most popular tourist area of Kathmandu. Thamel is a 15 to 20 minute walk from the center of Kathmandu. Thamel has clean narrow streets full of mushrooming lodges, hotels for budget travellers. Restaurants, bars and other tourists oriented shops can be seen bustling with activities</p> 
          
                	
          
         
        </div>
        <div class="col-md-3 col-sm-3">
        	<div class="primary-section">
            	<ul>
					
                   	<li><a href="img/themal4.jpg" class="popup-img"><img src="img/themal4.jpg" alt=""></a></li>
                   
                   
                  
				</ul>
            </div>
        </div>
        
      </div>
    </div>
  </div>
  <?php include('footer.php')?>