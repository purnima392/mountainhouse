<!--footer start-->
<footer class="footer">
  <div class="container"> 
  	<div class="row">
    	<div class="col-md-3 col-sm-3">
        	<div class="footer-content">
            	<div class="footer-logo"> <h4>About Us</h4>               <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta.</p>
                <a href="#" class="links">Read More</a>
                </div>
                <p class="copyright">© 2017. All rights reserved</p>   
                <p>Made with <span class="fa fa-heart"></span> by Webpage Nepal.</p>
                </div>
        </div>
        <div class="col-md-9 col-sm-9">
        	<div class="row">
            	
                <div class="col-md-4 col-sm-4">
                	<h4>Connect</h4>
                    <ul>
                    	<li><a href="#">+977-9846041887 / 9804193144 </a></li>
<li><a href="#">info@mountainhousetreks.com.np</a></li>
<li>Pokhara-6, Lakeside, Kaski, Nepal</li>

                    </ul>
                    <a href="#" class="tripadvisor"><img src="img/tripadvisor-logo.png" alt="tripadvisor"></a>
                </div>
                <div class="col-md-4 col-sm-4">
                	<h4>Social Media</h4>
                    <ul class="social-links">
                    	<li><a href="#"><i class="fa fa-linkedin"></i></a></li>
<li><a href="#"><i class="fa fa-google-plus"></i></a></li>
<li><a href="#"><i class="fa fa-twitter"></i></a></li>
<li><a href="#"><i class="fa fa-facebook"></i></a></li>
                    </ul>
                    <h4>AFFILIATED WITH</h4>
                    <ul class="holder">
                    	<li><a href="#"><img src="img/TAAN-white.png" alt="TAAN"></a></li>
						<li><a href="#"><img src="img/nepal-tourism-board-logo-white.png" alt="NTB"></a></li>
                    </ul>
                </div>
                <div class="col-md-4 col-sm-4">
                	<h4>Exchange Rate</h4>
                    <div class="currency-wrapper">
                    	<!-- Exchange Rates Script - EXCHANGERATEWIDGET.COM -->
<div style="width:228px;border:3px solid #2D6AB4;border-radius:5px;text-align:left;"><div style="text-align:left;background-color:#2D6AB4;width:100%;border-bottom:0px;height:25px; font-size:12px;font-weight:bold;padding:5px 0px;"><span style="margin-left:2px;background-image:url(//www.exchangeratewidget.com/flag.png); background-position: 0 -848px; width:100%; height:15px; background-repeat:no-repeat;padding-left:5px;"><a href="https://www.exchangeratewidget.com/" style="color:#FFFFFF; text-decoration:none;padding-left:22px;" rel="nofollow">Nepalese Rupee Exchange Rates</a></span></div><script type="text/javascript" src="//www.exchangeratewidget.com/converter.php?l=en&f=NPR&t=EUR,GBP,JPY,CAD,AUD,HKD,&a=1&d=F0F0F0&n=FFFFFF&o=000000&v=7"></script></div>
<!-- End of Exchange Rates Script -->
                    </div>
                </div>
            </div>
            
        </div>
    </div>
    
      </div>
</footer>
<!--footer start--> 
<script type="text/javascript" src="js/script.js"></script>
 </body>
</html>