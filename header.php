<!DOCTYPE HTML>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>The Mountain House Treks & Expedition Pvt.Ltd.</title>
<!-- Bootstrap -->

<link href="css/bootstrap.css" rel="stylesheet">
<link href="css/weather.css" rel="stylesheet">
<link href="css/font-awesome.css" rel="stylesheet">
<link href="css/pe-icon-7-stroke.css" rel="stylesheet">
<link href="css/ionicons.css" rel="stylesheet">
<!--<link href="css/select.css" rel="stylesheet">-->
<link href="css/magnific-popup.css" rel="stylesheet">
<link href="css/jquery-ui.css" rel="stylesheet">
<link href="css/select2.min.css" rel="stylesheet">
<link href="css/owl.carousel.css" rel="stylesheet">
<link rel='stylesheet' href='css/unite-gallery.css' type='text/css' />
<link href="css/style.css" rel="stylesheet">
<link href="css/responsive.css" rel="stylesheet">
<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700,800' rel='stylesheet' type='text/css'>
<link href="https://fonts.googleapis.com/css?family=Oswald:200,300,400,500,600,700" rel="stylesheet">
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
		<script src="js/html5shiv.js"></script>
		<script src="js/respond.min.js"></script>
	<![endif]-->

<script type="text/javascript" src="js/jquery-2.1.1.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script type="text/javascript" src="js/bootstrap.js"></script>
<script type="text/javascript" src="js/modernizr.js"></script>
<script type="text/javascript" src="js/jquery-ui.js"></script>
<script type="text/javascript" src="js/select2.min.js"></script>
<script src="js/jquery.fitvids.js"></script>
<script type="text/javascript" src="js/metcast.jquery.js"></script>

<!-- Isotope javascript -->
<script type="text/javascript" src="js/isotope.pkgd.min.js"></script>
<script type="text/javascript" src="js/jquery.magnific-popup.min.js"></script>
<!-- Owl carousel javascript -->
<script type="text/javascript" src="js/owl.carousel.js"></script> 
<script type='text/javascript' src='js/unitegallery.min.js'></script> 
<script type='text/javascript' src='js/ug-theme-tiles.js'></script> 

</head>
<body>
<!-- scrollToTop --> 
<!-- ================ -->
<div class="scrollToTop"><i class="fa fa-angle-up"></i></div>
<header class="header" data-spy="affix" data-offset-top="60" >
	<div class="top-header clearfix">
    	<div class="container">
        	<div class="top-header-left pull-left">
            	<ul>
                	<li><i class="pe-7s-call"></i>+977 9804193144 / 9846041887</li>
                    <li><i class="pe-7s-map-marker"></i><a href="#">info@mountainhousetreks.com.np,  anjanresham99@gmail.com</a></li>
                </ul>
            </div>
            <div class="top-header-right pull-right">
            <ul class="social-link">
                	<li><a href="#"><i class="fa fa-facebook"></i></a></li>
                    <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                    <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                </ul>
            	
                <ul class="header-right">
                	<li class="login"><a href="#">Login</a></li>
                    <li><a href="#">Register</a></li>
                </ul>
            </div>
        </div>
    </div>
  <div class="main-navigation">
    <div class="navbar navbar-inverse navbar-static-top">
      <div class="container"> <a class="navbar-brand" href="index.php"> <span class="logo-text">The Mountain House<span>Treks & Expedition</span></span></a>
        <div class="navbar-header">
          <button class="navbar-toggle" data-toggle="collapse" data-target=".navbarHeaderCollaspse"> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
        </div>
        <div class="collapse navbar-collapse navbarHeaderCollaspse">
          <ul class="nav navbar-nav navbar-right">
            <li><a href="index.php">Home</a></li>
            <li><a href="about.php">About Us</a></li>
            <!--<li class="dropdown mega-menu"> <a href="#" class="dropdown-toggle" data-toggle="dropdown">Activities</a>
              <ul class="dropdown-menu">
                <li>
                  <div class="row">
                    <div class="col-sm-3 col-md-2">
                      <h4><a href="#">Air Safari</a></h4>
                      <img src="img/paramotoring-India_1444651196.jpg" alt="image-1"> </div>
                    <div class="col-sm-3 col-md-2">
                      <h4><a href="bungeejump.php">Bungee Jump</a></h4>
                      <img src="img/ann_banner_3_trp127.jpg" alt="image-1"> </div>
                    <div class="col-sm-3 col-md-2">
                      <h4><a href="#">Camping</a></h4>
                      <img src="img/Camp-Trekking-Holidays.jpg" alt="image-1"> </div>
                    <div class="col-sm-3 col-md-2">
                      <h4><a href="#">Rafting</a></h4>
                      <img src="img/river-rafting-1_1444038399.jpg" alt="image-1"> </div>
                    <div class="col-sm-3 col-md-2">
                      <h4><a href="#">Rock Climbing</a></h4>
                      <img src="img/ann_banner_4_trp205.jpg" alt="image-1"> </div>
                    <div class="col-sm-3 col-md-2">
                      <h4><a href="#">Skiing</a></h4>
                      <img src="img/ann_banner_2_trp188.jpg" alt="image-1"> </div>
                  </div>
                </li>
              </ul>
            </li>-->
            <li class="dropdown"> <a href="#" class="dropdown-toggle" data-toggle="dropdown">Activities <span class="fa fa-angle-down"></span></a>
              <ul class="dropdown-menu">
                <li><a href="trekking.php">Trekking</a></li>
                <li><a href="#">Paragliding</a></li>
                <li><a href="#">Rafting</a></li>
                <li><a href="#">Bunjee Jump</a></li>
                <li><a href="#">Zip flyer</a></li>
                <li><a href="#">Cycling Tour</a></li>
                <li><a href="#">Ultralight Tour</a></li>
                <li><a href="#">Heli Tour</a></li>
                <li><a href="#">Canyoning</a></li>
              </ul>
            </li>
            <li class="dropdown"> <a href="#" class="dropdown-toggle" data-toggle="dropdown">Tours <span class="fa fa-angle-down"></span></a>
              <ul class="dropdown-menu">
                <li><a href="bardiya-national-park.php">Bardiya</a></li>
                <li><a href="chitwan-national-park.php">Chitwan</a></li>
                <li><a href="kathmandu.php">Kathmandu</a></li>
                <li><a href="pokhara.php">Pokhara</a></li>
	      <li><a href="lumbini.php">Lumbini</a></li>
              </ul>
            </li>
             <li><a href="gallery.php">Gallery</a></li>
              <li><a href="#">Testimonial</a></li>
            <li><a href="contact.php">Contact Us</a></li>
            <li>
              <form action="" class="search-form">
                <div class="form-group has-feedback">
                  <label for="search" class="sr-only">Search</label>
                  <input type="text" class="form-control" name="search" id="search" placeholder="Holiday Search">
                  <span class="fa fa-search form-control-feedback"></span> </div>
              </form>
            </li>
          </ul>
        </div>
      </div>
    </div>
  </div>
</header>
