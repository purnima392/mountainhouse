jQuery(document).ready(function($){
	
// Basic FitVids Test
        $(".video-wrapper").fitVids();
        // Custom selector and No-Double-Wrapping Prevention Test
        $(".video-wrapper").fitVids({ customSelector: "iframe[src^='http://youtube.com']"});
			$('#weather').metCast({
				key: '4vfnzcgsce3jrf549uycnrbf',
				location: 'London'
			});
			
		
		jQuery("#gallery").unitegallery();
		$( ".datepicker" ).datepicker();
		
    var place = ["--Select--", "Bhadrapur", "Bhairahawa", "Bharatpur", "Biratnagar", "Dhangadi", "Janakpur", "Kathmandu", "Nepalgunj", "Pokhara", "Simara", "Tumlingtar"];
	
	$(".fromplace").select2({
				  data: place
				});
	var place2 = ["--Select--","Mountain", "Bhadrapur", "Bhairahawa", "Bharatpur", "Biratnagar", "Dhangadi", "Janakpur", "Kathmandu", "Nepalgunj", "Pokhara", "Simara", "Tumlingtar"];		
	$(".toplace").select2({
				  data: place2
				});
				var number = ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12"];		
	$(".adult").select2({
				  data: number
				});
				$(".child").select2({
				  data: number
				});
				$(".infant").select2({
				  data: number
				});
				$(".no_people").select2({
				  data: number
				});
				 var nationality = ["--Select--","Afghan", "Albanian", "American", "American Samoan", "Andorran", "Angolan", "Anguillan", "Antiguan, Barbudan", "Argentine", "Armenian", "Aruban ", "Australian", "Austrian", "Azerbaijani", "Bahamian", "Bahraini", "Bangladesh", "Barbadian/Bajan", "Basotho", "Belarusian", "Belgian", "Belizean ", "Beninese", "Bermudian", "Bhutanese", "Bolivian", "Bosnian, Herzegovinian", "Brazilian", "British", "British Virgin Islander, Bruneian", "Bulgarian", "Burkinabe", "Burundi ", "Cambodian", "Cameroonian", "Canadian", "Cape Verdean", "Caymanian", "Central African", "Chadian", "Chilean", "Chinese", "Christmas Island", "Cocos Islander"];
	
	$(".nationality").select2({
				  data: nationality
				});
				var currency = ["--Select--","Indian Rupees", "United Arab Emirates Dirham", "British Pound Sterling", "European Union Euro", "Nepali Rupees", "US Dollar"];		
	$(".currency").select2({
				  data: currency
				});
				var packages = ["Select a Package", "Just snap Pokhara", "Flights Close to Fishtail Mountain", "Fly in Between Himalayas", "Air Trek of Mountains"];		
	$(".packages").select2({
				  data: packages
				});		
	
    			//$('input[name$="optionsRadios"]').click(function() {
//       if($(this).attr('id') == 'one_way') {
//            $('#arrival_date').hide();           
//       }
//
//       else {
//            $('#arrival_date').show();   
//       }
//  
//});

$('input[name=way]').click(function () {
	
    if (this.id == "one_way") {
        $(".arrival_date").fadeOut('1000');
    } else {
        $(".arrival_date").fadeIn('1000');
    }
});

  
    
			});
			//Owl carousel
		//-----------------------------------------------
		if ($('.owl-carousel').length>0) {
			$(".owl-carousel.carousel").owlCarousel({
				items: 4,
				autoPlay: 5000,
				pagination: false,
				navigation: true,
				navigationText: false
			});
			$(".owl-carousel.content-slider").owlCarousel({
				singleItem: true,
				autoPlay: 5000,
				navigation: false,
				navigationText: false,
				pagination: false
			});
			};

		//Show dropdown on hover only for desktop devices
		//-----------------------------------------------
		var delay=0, setTimeoutConst;
		if (Modernizr.mq('only all and (min-width: 768px)') && !Modernizr.touch) {
			$('.main-navigation .navbar-nav>li.dropdown, .main-navigation li.dropdown>ul>li.dropdown').hover(
			function(){
				var $this = $(this);
				setTimeoutConst = setTimeout(function(){
					$this.addClass('open').slideDown();
					$this.find('.dropdown-toggle').addClass('disabled');
				}, delay);

			},	function(){ 
				clearTimeout(setTimeoutConst );
				$(this).removeClass('open');
				$(this).find('.dropdown-toggle').removeClass('disabled');
			});
		};

		//Show dropdown on click only for mobile devices
		//-----------------------------------------------
		if (Modernizr.mq('only all and (max-width: 767px)') || Modernizr.touch) {
			$('.main-navigation [data-toggle=dropdown], .header-top [data-toggle=dropdown]').on('click', function(event) {
			// Avoid following the href location when clicking
			event.preventDefault(); 
			// Avoid having the menu to close when clicking
			event.stopPropagation(); 
			// close all the siblings
			$(this).parent().siblings().removeClass('open');
			// close all the submenus of siblings
			$(this).parent().siblings().find('[data-toggle=dropdown]').parent().removeClass('open');
			// opening the one you clicked on
			$(this).parent().toggleClass('open');
			});
		};
		// Magnific popup
		//-----------------------------------------------
		if (($(".popup-img").length > 0) || ($(".popup-iframe").length > 0) || ($(".popup-img-single").length > 0)) { 		
			$(".popup-img").magnificPopup({
				type:"image",
				gallery: {
					enabled: true,
				}
			});
			$(".popup-img-single").magnificPopup({
				type:"image",
				gallery: {
					enabled: false,
				}
			});
			$('.popup-iframe').magnificPopup({
				disableOn: 700,
				type: 'iframe',
				preloader: false,
				fixedContentPos: false
			});
		};	
		// Isotope filters
		//-----------------------------------------------
		if ($('.isotope-container').length>0 || $('.masonry-grid').length>0 || $('.masonry-grid-fitrows').length>0) {
			$(window).load(function() {
				$('.masonry-grid').isotope({
					itemSelector: '.masonry-grid-item',
					layoutMode: 'masonry'
				});
				$('.masonry-grid-fitrows').isotope({
					itemSelector: '.masonry-grid-item',
					layoutMode: 'fitRows'
				});
				$('.isotope-container').fadeIn();
				var $container = $('.isotope-container').isotope({
					itemSelector: '.isotope-item',
					layoutMode: 'masonry',
					transitionDuration: '0.6s',
					filter: "*"
				});
				// filter items on button click
				$('.filters').on( 'click', 'ul.nav li a', function() {
					var filterValue = $(this).attr('data-filter');
					$(".filters").find("li.active").removeClass("active");
					$(this).parent().addClass("active");
					$container.isotope({ filter: filterValue });
					return false;
				});
			});
		};
		// Isotope filters
		//-----------------------------------------------
		if ($('.isotope-grid').length>0 || $('.masonry-grid').length>0 || $('.masonry-grid-fitrows').length>0) {
			$(window).load(function() {
				$('.masonry-grid').isotope({
					itemSelector: '.masonry-grid-item',
					layoutMode: 'masonry'
				});
				$('.masonry-grid-fitrows').isotope({
					itemSelector: '.masonry-grid-item',
					layoutMode: 'fitRows'
				});
				$('.isotope-grid').fadeIn();
				var $container = $('.isotope-grid').isotope({
					itemSelector: '.grid-item',
					layoutMode: 'masonry',
					transitionDuration: '0.6s',
					filter: "*"
				});
				// filter items on button click
				$('.trek-filters').on( 'click', 'ul.nav li a', function() {
					var filterValue = $(this).attr('data-filter');
					$(".trek-filters").find("li.active").removeClass("active");
					$(this).parent().addClass("active");
					$container.isotope({ filter: filterValue });
					return false;
				});
			});
			//Scroll totop
		//-----------------------------------------------
		$(window).scroll(function() {
			if($(this).scrollTop() != 0) {
				$(".scrollToTop").fadeIn();	
			} else {
				$(".scrollToTop").fadeOut();
			}
		});
		
		$(".scrollToTop").click(function() {
			$("body,html").animate({scrollTop:0},800);
		});
		
		};



			
		
			
			
  
	   
			
	

  
   
  