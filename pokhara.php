<?php include('header.php')?>

<div class="pagetop">
	<div class="container">
    	<div class="row">
        	<div class="col-md-6 col-sm-6">
            	<h2>Pokhara Tour Package</h2>
            </div>
            <div class="col-md-6 col-sm-6">
            	<ol class="breadcrumb pull-right">
  <li><a href="index.php">Home</a></li>
  <li class="active">Pokhara Tour Package</li>
</ol>
            </div>
        </div>
    </div>
</div>
  
  <div class="main page-wrapper">
    <div class="container">
      <div class="row">
        <div class="col-md-9 col-sm-9">
         <p>Pokhara is Nepal is number 1 adventure and leisure city, a trekking gateway to the Annapurna is with plenty of entertainment for individual travellers as well as families with kids. In Pokhara you can experience the best in trekking, boating, hiking, pony rides, paragliding or simply relax at one of the lakes near the city with the stunning Annapurna mountain range at the background. Pokhara will overwhelm you with one of the greatest photo spots in the world. While you walk along the shores of the Fewa lake you will be surrounded by a panoramical view over the Himalayan peaks of the Annapurna Massif that reflect in this lake. </p><p>A beautiful color combination that is created by the wooden boats and paragliders that are sailing, flying and rowing over the lake is probably going to be one of the highlights of your travel experience in Nepal. Trekking in the Annapurna from Pokhara Pokhara is well-known as a starting point for various trekking trail and expeditions in the Annapurna . Most of the hikers and trekkers for Annapurna and Ghandruk, respectively, build Pokhara as their first stop, or as relaxing station before they head out for serious walking. So, one may enjoy boating and reading books observing the serenity of the nature, or enjoy a couple of drinks, either in local restaurants or at a blues bar, or may simply enjoy sightseeing or cycling around the city. There are plenty of souvenir shops in the market section of the city. </p><p>Pokhara produces some of the finest handicrafts in the country. There are many activities to do in the city like, short hike to Sarangkot is highly recommended, which offers the magnificent sight of the sunset, sunrise, and of the whole city beneath. Davis Waterfall, a sublime waterfall in the city is another excursion not to be missed.</p>
        </div>
        <div class="col-md-3 col-sm-3">
        	<div class="primary-section">
            	<ul>
					<li><a href="img/pokhara1.jpg" class="popup-img"><img src="img/pokhara1.jpg" alt=""></a></li>
                    <li><a href="img/pokhara4.jpg" class="popup-img"><img src="img/pokhara4.jpg" alt=""></a></li>
                    <li><a href="img/pokhara6.jpg" class="popup-img"><img src="img/pokhara6.jpg" alt=""></a></li>
                    <li><a href="img/pokhara7.jpg" class="popup-img"><img src="img/pokhara7.jpg" alt=""></a></li>
                    
                   
				</ul>
            </div>
        </div>
        
      </div>
    
    </div>
  </div>
  <?php include('footer.php')?>