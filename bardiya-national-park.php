<?php include('header.php')?>
<div class="pagetop">
	<div class="container">
    	<div class="row">
        	<div class="col-md-6 col-sm-6">
            	<h2>Bardiya National Park</h2>
            </div>
            <div class="col-md-6 col-sm-6">
            	<ol class="breadcrumb pull-right">
  <li><a href="index.php">Home</a></li>
  <li class="active">Bardiya National Park</li>
</ol>
            </div>
        </div>
    </div>
</div>
  
  <div class="main page-wrapper">
    <div class="container">
    
    <div class="row">
        	<div class="col-md-9 col-sm-9">
            	<div class="content-box">
                	
                     <p>Bardia National Park is one of Nepal is best kept secrets. Located in the Terai region it is Nepal is largest national park and wilderness area, protecting 968 kmÂ² of sal forest, grassland, savannah and riverine forest. On the west side it is bordered by the Karnali River and it is bisected by the Babai River in the Bardiya District. The foot of the Siwalik Hills marks the northern boundary of the park. It is not overrun by tourists and there are excellent opportunities to spot endangered species of wildlife. The king of Bardia is the Bengal tiger. The tiger population is slowly increasing and counts around 50 to 60 animals. But Bardia is also the habitat for the wild Asian elephant and the greater one-horned rhinoceros. Bardia National Park is the largest national park and wilderness area in the Terai and has excellent wildlife-watching opportunities. Bardia is often described as what Chitwan was like 30 years ago, healthy populations of wild elephants and one-horned rhinos, Royal Bangal Tiger among the 30 species of mammals living here his trek is available on a private itinerary basis to suit your own dates and that are guaranteed to operate with a minimum of 1 or 2 people in the group.</p>
                    
                    <table class="table table-bordered">
                  <tbody>
                    <tr>
                      <th colspan="2"><h3>2 Nights 3 Days package Tour</h3></th>
                    </tr>
                    <tr>
                      <td colspan="2" bgcolor="#e1f3d0"><h3>
                         Day 01
                        </h3></td>
                    </tr>
                    <tr>
                      <td colspan="2">On Your arrival at <em><strong>Nepalgunj</strong></em> our staff will receive you. Transfer to the resort/hotel. Refreshment drink and briefing about the accommodation and the facilities. After lunch our manager will brief you about the program of the following days. Visit to crocodile breeding center and tharu village & Jungle drive. You enjoy dinner, in the evening; enjoy cultural programs conducted by Tharu, community of Terai. You stay overnight at camp or lodge.</td>
                    </tr>
                    <tr class="even">
                      <td width="50">13:00</td>
                      <td><div align="left">Lunch.</div></td>
                    </tr>
                    <tr>
                      <td>15:30</td>
                      <td><div align="left"><em>Crocodile breeding center</em> /Tharu village/Jungle Jeep drive</div></td>
                    </tr>
                    <tr class="even">
                      <td>17:00</td>
                      <td><div align="left">Sunset view from Riverbank</div></td>
                    </tr>
                    <tr class="even">
                      <td>18:30</td>
                      <td><div align="left">Slide show/Tharu stick dance</div></td>
                    </tr>
                    <tr class="even">
                      <td>19:00</td>
                      <td><div align="left">Dinner/Culture program</div></td>
                    </tr>
                    <tr class="even">
                      <td colspan="2" bgcolor="#e1f3d0"><h3>
                          Day 02
                          
                        </h3></td>
                        
                    </tr>
                    <tr>
                      <td colspan="2">In <em>Bardia National Park</em>, you enjoy a full day of Wild-Life Safari activities such as: Elephant back safari, Nature walks, Canoe trip or boat ride, Jeep drive in the Deep forest, Elephant bathing, bird watching, Tharu Village tour etc. as per time permits. Trained local naturalists and guides will guide all to you. Animals and birds to be seen are rhinoceros, crocodiles, bear, monkeys, several species of deer, pythons, peacocks, hornbills, woodpeckers (amongst more than 400 species of birds recorded in the park).</td>
                    </tr>
                    <tr class="even">
                      <td width="50">05:30</td>
                      <td><div align="left">Wake up call.</div></td>
                    </tr>
                    <tr class="even">
                      <td>05:45</td>
                      <td><div align="left">Breakfast.</div></td>
                    </tr>
                    <tr class="even">
                      <td>06:00</td>
                      <td><div class="align-left">Elephant ride/bird watching</div></td>
                    </tr>
                    <tr class="even">
                      <td>8:00</td>
                      <td><div align="left">Breakfast</div></td>
                    </tr>
                    <tr class="even">
                      <td>9:00</td>
                      <td><div align="left">Canoe ride/elephant briefing / swimming and bath</div></td>
                    </tr>
                    <tr class="even">
                      <td>13:00</td>
                      <td><div align="left">Lunch</div></td>
                    </tr>
                    <tr class="even">
                      <td>15:00</td>
                      <td><div align="left">Elephant ride / nature walk / visit to observation tower / Canoe ride</div></td>
                    </tr>
                    <tr class="even">
                      <td>18:30</td>
                      <td><div align="left">Tharu Stick Dance/slide show</div></td>
                    </tr>
                    <tr class="even">
                      <td>19:30</td>
                      <td><div align="left">Dinner</div></td>
                    </tr>
                    <tr class="even">
                      <td colspan="2" bgcolor="#e1f3d0"><h3>
                          Day 03
                          
                        </h3></td>
                    </tr>
                    <tr>
                      <td colspan="2">After tea or coffee you will go for a bird watching tour. B/F. Transfer to the bus station or the <em>Nepalgunj Airport</em> for your onward departure.</td>
                    </tr>
                     <tr class="even">
                      <td width="50">05:30</td>
                      <td><div align="left">Wake up call.</div></td>
                    </tr>
                    <tr class="even">
                      <td>05:45</td>
                      <td><div align="left">Breakfast.</div></td>
                    </tr>
                    <tr class="even">
                      <td>06:00</td>
                      <td><div class="align-left">Bird watching Tour</div></td>
                    </tr>
                    <tr class="even">
                      <td>8:00</td>
                      <td><div align="left">Breakfast/Transfer to Bus station/airport</div></td>
                    </tr>
                    <tr class="even">
                      <td>9:00</td>
                      <td><div align="left">Final departure</div></td>
                    </tr>
                    
                  </tbody>
                </table> 
                    
                </div>
            </div>
            <div class="col-md-3 col-sm-3">
        	<div class="primary-section">
            	<ul>
					<li><a href="img/bardiya.jpg" class="popup-img"><img src="img/bardiya.jpg" alt=""></a></li>
                    <li><a href="img/bardiya2.jpg" class="popup-img"><img src="img/bardiya2.jpg" alt=""></a></li>
                    <li><a href="img/bardiya4.jpg" class="popup-img"><img src="img/bardiya4.jpg" alt=""></a></li>
                    <li><a href="img/bardiya5.jpg" class="popup-img"><img src="img/bardiya5.jpg" alt=""></a></li>
                  
                      <li><a href="img/bardiya6.jpg" class="popup-img"><img src="img/bardiya6.jpg" alt=""></a></li>
                       <li><a href="img/bardiya7.jpg" class="popup-img"><img src="img/bardiya7.jpg" alt=""></a></li>
				</ul>
            </div>
        </div>
            
        </div>
      
    </div>
  </div>
  <?php include('footer.php')?>