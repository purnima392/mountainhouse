<?php include('header.php')?>
  <div class="pagetop">
	<div class="container">
    	<div class="row">
        	<div class="col-md-6 col-sm-6">
            	<h2>Bungee Jump</h2>
            </div>
            <div class="col-md-6 col-sm-6">
            	<ol class="breadcrumb pull-right">
  <li><a href="index.php">Home</a></li>
  <li class="active">Bungee Jump</li>
</ol>
            </div>
        </div>
    </div>
</div>
  
 <div class="main">
		<div class="container">
        <div class="row">
        	<div class="col-md-6 col-sm-6">
            	<div class="content-box">
                	
                    <p>Pokhara is famous for its mountains, lakes, views, friendly people and as an ideal destination for adventure enthusiasts. The range of adventure sports activities on offer in Pokhara is sure to satisfy every adventure lover. You can try trekking and mountaineering in the rugged landscape which offers many routes for trekkers and relax and wind off your trek by the lake with most amazing views of lakes and sunsets. Tour to Pokhara is sure to make a thrill seeker happy with lots of options for adventure sports in the country.</p>
                    
                    
                     
                    
                </div>
            </div>
            <div class="col-md-6 col-sm-6">
            <img src="img/bungee-720x470.jpg" alt="">
            </div>
        </div>
         <hr/>
          <div class="row clearfix">
          	<div class="col-md-8">
            	<div class="video-wrapper">
                	<div class="vendor">
                   <iframe width="425" height="250" src="https://www.youtube.com/embed/nNd7koPYixI" frameborder="0"></iframe>
        	
           
	      </div>
                </div>
            </div>
            
            <div class="col-md-4">
            	<img src="img/bungee-720x470.jpg" alt="">
                <hr/>
                <img src="img/Pokhara-Bungy-Jump.jpg" alt="">
            </div>
          </div>
        
		</div>
	</div>
  <?php include('footer.php')?>