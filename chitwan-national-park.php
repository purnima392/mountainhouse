<?php include('header.php')?>

<div class="pagetop">
	<div class="container">
    	<div class="row">
        	<div class="col-md-6 col-sm-6">
            	<h2>Chitwan National Park</h2>
            </div>
            <div class="col-md-6 col-sm-6">
            	<ol class="breadcrumb pull-right">
  <li><a href="index.php">Home</a></li>
  <li class="active">Chitwan National Park</li>
</ol>
            </div>
        </div>
    </div>
</div>
  
  <div class="main page-wrapper">
    <div class="container">
      <div class="row">
        <div class="col-md-9 col-sm-9">
          
          
          
          
          <p> The district takes its name from the Chitwan Valley, one of Nepal is Inner Terai valleys between the Mahabharat and Siwalik ranges, both considered foothills of the Himalayas. Narayangadh is located on the banks of the Narayani River, and is the main town with numerous shopping zones where people come from all over the district and neighbouring districts. Meaning Heart of the Jungle, Chitwan is famous as one of the best wildlife-viewing national parks in Asia, and you will have an excellent chance of spotting one-horned rhinos, deer, monkeys and up to 544 species of birds. If you are extremely lucky, you will see leopards, wild elephants and sloth bears â€“ though it is the once-in-a-lifetime chance to spot a majestic royal Bengal tiger that attracts people in their droves. the best option for experiencing Chitwan National Park is to stay in one of the luxury lodges located on the edge of the park away from the crowds at Sauraha. The government stopped the luxury lodges from operating inside the park in 2012. Most of these lodges have developed alternative lodges just outside the national park boundaries. Though this experience does not come cheaply, the sense of adventure, less crowding and access to more remote parts of the park make it very worthwhile. wo whole days in the park is really the minimum for wildlife spotting. The nature of dense jungle, tall grass and the nocturnal hours kept by many animals are all factors that make spotting animals far from guaranteed. A good approach is to treat wildlife viewing as one would the pastime of fishing: some days youâ€™ll get plenty of bites, others not a nibble.</p>
                <table class="table table-bordered">
                  <tbody>
                    <tr>
                      <th colspan="2"><h3>3 Nights 4 Days package Tour</h3></th>
                    </tr>
                    <tr>
                      <td colspan="2" bgcolor="#e1f3d0"><h3>
                         Day 01
                        </h3></td>
                    </tr>
                    <tr>
                      <td colspan="2">On Your arrival at <em><strong>Sauraha Bus park, Tadi </strong></em>or<em><strong> Bharatpur Airport</strong></em> our staff will receive you. You will then be transferred to our Resort where you will 
                        be offered refreshment &amp; be given a briefing 
                        &amp; its facilities. After being given your 
                        programmes for the day, your room will then 
                        be allocated to you.</td>
                    </tr>
                    <tr class="even">
                      <td width="50">13:00</td>
                      <td><div align="left">Lunch.</div></td>
                    </tr>
                    <tr>
                      <td>15:00</td>
                      <td><div align="left"><em>Village tour</em> to a nearby ethnic Tharu village where you will learn more about the life and lifestyle of the Tharus. Visit to the <em><strong>National Park Visitor's </strong></em>centre where you can learn more about the history of the National Park and about wildlife &amp; Sun set view from the bank of Rapti River.</div></td>
                    </tr>
                    <tr class="even">
                      <td>19:00</td>
                      <td><div align="left"><strong><em>Tharu cultural dance presentation</em></strong> (which you will also participate in) by the <em><strong>local villagers</strong></em>.</div></td>
                    </tr>
                    <tr class="even">
                      <td>20:00</td>
                      <td><div align="left">Dinner.</div></td>
                    </tr>
                    <tr class="even">
                      <td colspan="2" bgcolor="#e1f3d0"><h3>
                          Day 02
                          
                        </h3></td>
                    </tr>
                    <tr class="even">
                      <td width="50">06:00</td>
                      <td><div align="left">Wake up call.</div></td>
                    </tr>
                    <tr class="even">
                      <td>06:30</td>
                      <td><div align="left">Breakfast.</div></td>
                    </tr>
                    <tr class="even">
                      <td>07:00</td>
                      <td><div align="left"><strong><em>Canoe ride</em></strong> along the <em><strong>Rapti River</strong></em>. An excellent opportunity for <em><strong>Bird Watching</strong></em> and for seeing the <em> <strong>2 rare species of crocodiles</strong></em>; the <em><strong>Marsh Mugger</strong></em> and the <em><strong>fish eating Gharial</strong></em>. + <em><strong>Jungle Walk</strong></em> + on the way back one can enjoy the <em><strong>elephant bathing</strong></em>. </div></td>
                    </tr>
                    <tr class="even">
                      <td>13:00</td>
                      <td><div align="left">Lunch</div></td>
                    </tr>
                    <tr class="even">
                      <td>15:00</td>
                      <td><div align="left"><em><strong>Jeep Safari </strong></em> <em><strong></strong></em> or <em><strong>Elephant                                       Safari</strong></em> or <em><strong>Jungle Walk</strong></em></div></td>
                    </tr>
                    <tr class="even">
                      <td>20:00</td>
                      <td><div align="left">Dinner</div></td>
                    </tr>
                    <tr class="even">
                      <td colspan="2" bgcolor="#e1f3d0"><h3>
                          Day 03
                          
                        </h3></td>
                    </tr>
                    <tr class="even">
                      <td width="50">06:30</td>
                      <td><div align="left">Wake up call.</div></td>
                    </tr>
                    <tr class="even">
                      <td>07:00 </td>
                      <td><div align="left">Breakfast.</div></td>
                    </tr>
                    <tr class="even">
                      <td>08:00</td>
                      <td><div align="left"><em><strong>Visit to the Government Elephant                                       Breeding Centre.</strong></em></div></td>
                    </tr>
                    <tr class="even">
                      <td>13:00</td>
                      <td><div align="left">Lunch</div></td>
                    </tr>
                    <tr class="even">
                      <td>14:00</td>
                      <td><div align="left"><strong><em>Elephant                                       Safari</em></strong>. An excellent opportunity                                       to see <em><strong>four different kinds of                                       deer, rhinoceros, wild boar, monkey, leopard,                                       sloth bear</strong></em> and the <em><strong>Royal                                       Bengal Tiger</strong></em> (If you are lucky).                                       You will also encounter many other smaller                                       mammals that have made <em><strong>Chitwan</strong></em> their home.</div></td>
                    </tr>
                    <tr class="even">
                      <td>20:00</td>
                      <td><div align="left">Dinner</div></td>
                    </tr>
                    <tr class="even">
                      <td colspan="2" bgcolor="#e1f3d0"><h3>
                          
                         Day 04
                          
                        </h3></td>
                    </tr>
                    <tr class="even">
                      <td width="50">07:00</td>
                      <td><div align="left">Wake up call.</div></td>
                    </tr>
                    <tr class="even">
                      <td>08:00 </td>
                      <td><div align="left">Breakfast.</div></td>
                    </tr>
                    <tr class="even">
                      <td>09:00</td>
                      <td><div align="left">Departure for onward journey.</div></td>
                    </tr>
                  </tbody>
                </table>	
          
         
        </div>
        <div class="col-md-3 col-sm-3">
        	<div class="primary-section">
            	<ul>
					<li><a href="img/chitwan1.jpg" class="popup-img"><img src="img/chitwan1.jpg" alt=""></a></li>
                    <li><a href="img/chitwan2.jpg" class="popup-img"><img src="img/chitwan2.jpg" alt=""></a></li>
                    <li><a href="img/chitwan3.jpg" class="popup-img"><img src="img/chitwan3.jpg" alt=""></a></li>
                    <li><a href="img/chitwan4.jpg" class="popup-img"><img src="img/chitwan4.jpg" alt=""></a></li>
                  
                      <li><a href="img/chitwan6.jpg" class="popup-img"><img src="img/chitwan6.jpg" alt=""></a></li>
                       <li><a href="img/chitwan7.jpg" class="popup-img"><img src="img/chitwan7.jpg" alt=""></a></li>
				</ul>
            </div>
        </div>
        
      </div>
    </div>
  </div>
  <?php include('footer.php')?>