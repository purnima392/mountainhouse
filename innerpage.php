<?php include('header.php');
      require('AdminLTE/inc/config.php');
      $PackageId=$_GET['id'];
      $latPackage=$mysqli->query("select * from packages where PackageId=$PackageId");
      $SiPackage=$latPackage->fetch_array();
      $CategoryId=$SiPackage["CategoryId"];
      $PackageId=$SiPackage["PackageId"];
      $Title=$SiPackage["Title"];
      $Description=$SiPackage["Description"];
      $Photo=$SiPackage["Photo"];
      $Facts=$SiPackage["Facts"];
      $DetailItenary=$SiPackage["DetailItenary"];
      $CostInclude=$SiPackage["CostInclude"];
      $CostExclude=$SiPackage["CostExclude"];
      $Notes=$SiPackage["Notes"];
      $Cancellation=$SiPackage["Cancellation"];
      $Duration=$SiPackage["Duration"];
?>
<div class="main inner">
  <div class="container">
    <ol class="breadcrumb">
      <li><a href="index.php">Home</a></li>
      <li><a href="trekking.php">Trekking</a></li>
      <li class="active"><?=$Title?></li>
    </ol>
    <div class="row top-block">
      <div class="col-md-8">
        <div class="slide-top">
          <div class="slideshow">
            <div class="slide"> <img src="img/trekking/<?=$Photo?>" alt="img description" class="img-responsive"> </div>
          </div>
          <div class="slide-description">
           <div class="add-info"> 
	  <span class="country-name"><a href="#">Nepal</a></span> 
            <span class="time"><?=$Duration?></span> </div>
            <strong><?=$Title?></strong> 
	  <a href="booking-form.php?id=<?=$PackageId?>">Book Now</a>
	 </div>
        </div>
        <div class="panel-group article" id="accordion" role="tablist" aria-multiselectable="true">
          <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="headingOne">
              <h4 class="panel-title"> <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">Trip <strong>Overview</strong></a> </h4>
            </div>
            <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
              <div class="panel-body">
                <?=$Description?>
              </div>
            </div>
          </div>
          <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="headingTwo">
              <h4 class="panel-title"> <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">Detail <strong>Itenary</strong></a> </h4>
            </div>
            <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
              <div class="panel-body">
                <?=$DetailItenary?>
              </div>
            </div>
          </div>
          <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="headingThree">
              <h4 class="panel-title"> <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">Inclusions & <strong>Exclusions</strong></a> </h4>
            </div>
            <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
              <div class="panel-body"> <strong>The Trip cost will be vary depending on the Group size duration of days and Services required please contact us Via our email <a href="#">info@mountainhousetreks.com.np</a> with your details to obtain a quote.</strong>
                <div class="row">
                  <div class="col-md-6">
                    <h3>Includes:</h3>
                    <?=$CostInclude?>
                  </div>
                  <div class="col-md-6">
                    <h3>Excludes:</h3>
                   <?=$CostExclude?>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="article">
          <div class="article-post">
            <h2>Trip <strong>Facts</strong></h2>
            <div class="detail">
              <div class="facts-module">
	     <div class="box">
	     <?=$Facts?>			
	     </div>
	    </div>
            </div>
          </div>
          
          
        </div>
      </div>
      <div class="col-md-4">
        <div class="adsbanner no-margin"> <a href="#"><img src="img/dunhill300x250a-1.gif" alt=""></a> </div>
        <div class="adsbanner"><a href="#"><img src="img/adsbanner1.jpg" alt=""></a></div>
        <div class="adsbanner"><a href="#"><img src="img/adsbanner2.jpg" alt=""></a></div>
      </div>
    </div>
    <div class="related-deals">
      <h2><a href="#">Related</a> deals</h2>
      <div class="row">
        <div class="col-lg-4 col-sm-6 grid-item everest">
          <div class=" deal-post hotel">
            <div class="img-holder"> <a href="#"><img src="img/makalu-view-from-the-chhukhung-ri_640_480.jpg" alt="img description" class="img-responsive"></a>
              <div class="btn-wrapper"><a href="#" class="btn">View Detail</a></div>
            </div>
            <div class="deal-description"> <a href="#"><strong>Everest Base Camp</strong></a>
              <p>Ever since the first expedition to the Everest in 1953 from the side of Nepal, the Mount Everest base camp has been a popular destination to the trekkers.</p>
            </div>
          </div>
        </div>
        <div class="col-lg-4 col-sm-6 grid-item annapurna">
          <div class="deal-post flight">
            <div class="img-holder"> <a href="#"><img src="img/jomsom-muktinath-trek1469171412.jpg" alt="img description" class="img-responsive"></a>
              <div class="btn-wrapper"><a href="#" class="btn">View Detail</a></div>
            </div>
            <div class="deal-description"> <a href="#"><strong>Annapurna Circuit</strong></a>
              <p>Annapurna trek takes you through the most classic trekking around the Annapurna region. </p>
            </div>
          </div>
        </div>
        <div class="col-lg-4 col-sm-6 grid-item holy">
          <div class="deal-post activities">
            <div class="img-holder"> <a href="#"><img src="img/d51.jpg" alt="img description" class="img-responsive"></a> <a href="#"></a>
              <div class="btn-wrapper"><a href="#" class="btn">View Detail</a></div>
            </div>
            <div class="deal-description"> <a href="#"><strong>Holy Tour</strong></a>
              <p>Kathmandu has often been by some as a flawless jewel in a unique setting. </p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<?php include('footer.php')?>
