<?php include('header.php')?>
  <div class="pagetop">
	<div class="container">
    	<div class="row">
        	<div class="col-md-6 col-sm-6">
            	<h2>Rafting</h2>
            </div>
            <div class="col-md-6 col-sm-6">
            	<ol class="breadcrumb pull-right">
  <li><a href="index.php">Home</a></li>
  <li class="active">Rafting</li>
</ol>
            </div>
        </div>
    </div>
</div>
  
  <div class="main">
		<div class="container">
        <div class="row">
        	<div class="col-md-6 col-sm-6">
            	<div class="content-box">
                	
                    <p>The Trishuli is the most popular river rafting destination in Nepal as it offers great scenery, exciting rapids and impressive gorges. White water river raft in Trishuli offers the great option of a day rafting in Nepal. A day river rafting in Trishuli can be organized through the year as it is easily accessible from Kathmandu.</p>

<p>The Rafting in Trishuli features Class III + rapids; with our trained guides at the helm, rafting group will safely navigate the most tumultuous bends and challenging chutes. You will be navigated through famous rapids like – Upset, Ladies’ Delight, Surprise, S Bend and Pinball. Rafting on the Trishuli River provides enough action to keep keen whitewater veterans on the edges of their seats: surprisingly, though, is safe for children and first-time rafters as well.</p>

<p>We organize transportation from your hotel at 7:00 in morning to the beginning of this rafting adventure at Charaudi – the point at which the rafts are put on the river – this drive takes around 2.30 hours from Kathmandu. After having the rafting preparation,orientation and safety briefing ,we start our 3 hour breathtaking rafting adventure down the Trishuli River: our rafting ends by 3:00 pm at Kurintar. From here we can drive back to Kathmandu. You, also, have the option to drive to Pokhara or Chitwan after a day’s rafting trip on the Trishuli river.</p>
                    
                    
                     
                    
                </div>
            </div>
            <div class="col-md-6 col-sm-6">
            <img src="img/seti-river-rafting.jpg" alt="">
            </div>
        </div>
         <hr/>
          <div class="row clearfix">
          	<div class="col-md-8">
            	<div class="video-wrapper">
                	<div class="vendor">
                   <iframe width="560" height="315" src="https://www.youtube.com/embed/O_f9-0_CGgc" frameborder="0" allowfullscreen></iframe>
        	
           
	      </div>
                </div>
            </div>
            
            <div class="col-md-4">
            	<img src="img/Kali-gandaki-rafting.jpg" alt="">
                <hr/>
                <img src="img/tour_img-375103-145.jpg" alt="">
            </div>
          </div>
          
        
		</div>
	</div>
  
  
  
 
  <?php include('footer.php')?>