<?php include('header.php');
	require('AdminLTE/inc/config.php');
?>
<div class="main">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="related-deals">
					<h2>Best Selling <a href="#">Packages</a> </h2>
					<div class="isotope-container row">
						<?php 
							$Packages=$mysqli->query("SELECT * FROM packages");
							while($SiPackage=$Packages->fetch_array()){
								$PackageId=$SiPackage["PackageId"];
								$Title=$SiPackage["Title"];
								$Description=$SiPackage["Description"];
								$Photo=$SiPackage["Photo"];
								$Duration=$SiPackage["Duration"];
							?>
							<div class="col-lg-3 col-sm-4 isotope-item makalu">
								<div class=" deal-post hotel">
									<div class="img-holder"> 
										<a href="#">
										<img src="img/trekking/<?=$Photo?>" alt="img description" class="img-responsive"></a> 
										<div class="btn-wrapper"><a href="innerpage.php?id=<?=$PackageId?>" class="btn">View Detail</a></div>
									</div>
									<div class="deal-description"> <a href="#"><strong><?=$Title?> - <?=$Duration?></strong></a>
										<p><?=$Description?></p>
									</div>
								</div>
							</div>
						<?php } ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<?php include('footer.php')?>
