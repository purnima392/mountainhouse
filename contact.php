<?php include('header.php')?>
<div class="pagetop">
	<div class="container">
    	<div class="row">
        	<div class="col-md-6 col-sm-6">
            	<h2>Contact Us</h2>
            </div>
            <div class="col-md-6 col-sm-6">
            	<ol class="breadcrumb pull-right">
  <li><a href="index.php">Home</a></li>
  <li class="active">Contact Us</li>
</ol>
            </div>
        </div>
    </div>
</div>

	<div class="main">
		<div class="container">
        <div class="row">
        	<div class="col-md-6 col-sm-6">
            	<div class="content-box contact-page">
                	<div class="page-title">
                    	
                        <h3>Get in Touch</h3>
                        <p class="lead">Please fill up the form</p>
                    </div>
                    <form class="form-horizontal">
  <div class="form-group">
    <label for="inputEmail3" class="col-sm-4 control-label">Full Name:</label>
    <div class="col-sm-8 inner-input">
    <i class="pe-7s-user"></i>
      <input type="text" class="form-control" id="fullname" placeholder="Enter Full Name...">
    </div>
  </div>
  <div class="form-group">
    <label for="phoneno" class="col-sm-4 control-label">Phone Number:</label>
    <div class="col-sm-8 inner-input">
    <i class="pe-7s-call"></i>
      <input type="text" class="form-control" id="phoneno" placeholder="Enter Phone Number">
    </div>
  </div>
  <div class="form-group">
    <label for="email" class="col-sm-4 control-label">Email Address:</label>
    <div class="col-sm-8 inner-input">
    <i class="pe-7s-mail"></i>
      <input type="email" class="form-control" id="email" placeholder="Enter Email Address">
    </div>
  </div>
  <div class="form-group">
    <label for="subject" class="col-sm-4 control-label">Subject:</label>
    <div class="col-sm-8 inner-input">
    <i class="pe-7s-note"></i>
      <input type="text" class="form-control" id="subject" placeholder="Enter Subject">
    </div>
  </div>
  <div class="form-group">
    <label for="message" class="col-sm-4 control-label">Message:</label>
    <div class="col-sm-8 inner-input">
    <i class="pe-7s-comment"></i>
    <textarea style="text" rows="3" class="form-control" placeholder="Enter Your Message Here..."></textarea>
     
    </div>
  </div>
  <div class="form-group">
    <div class="col-sm-offset-4 col-sm-9">
      <button type="submit" class="btn btn-default">Sign in</button>
    </div>
  </div>
</form>
                    <div class="page-title">
                    	
                        <h3>Contact Us</h3>
                    </div>
                    <div class="contact-address">
                    	<ul>
                        	<li><i class="pe-7s-map-2"></i>
                            	Pokhara-6, Lakeside Kaski, Nepal
                            </li>
                            <li><i class="pe-7s-phone"></i>
                            	<a href="#">+977-9846041887 </a>, <a href="#">+977-9804193144 </a>
                            </li>
                            <li><i class="pe-7s-mail-open"></i>
                            	<a href="#">info@mountainhousetreks.com.np</a>,
			<a href="#">anjanresham@hotmail.com</a>,
			<a href="#">anjanresham99@gmail.com</a>
			</a>
                            </li>
                            <li><i class="pe-7s-ball"></i>
                            	<a href="#">www.mountainhousetreks.com.np</a>
                            </li>
                        </ul>
                    </div>
                    
                     
                    
                </div>
            </div>
            <div class="col-md-6 col-sm-6">
            <div class="map-canvas">
            	<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3515.8383986962267!2d83.95764731455783!3d28.212221982585504!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3995951ded28b6c1%3A0x6bd988812a46afe6!2sThe+Mountain+House+Treks+%26+expedition+Pvt.Ltd.!5e0!3m2!1sen!2snp!4v1503049927968" width="100%" height="700" frameborder="0" style="border:0" allowfullscreen></iframe>
            </div>
            	
                
            </div>
        </div>
        
		</div>
	</div>
<?php include('footer.php')?>    