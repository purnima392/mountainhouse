<?php include('header.php')?>
<div class="banner">
		<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
  <!-- Indicators -->
  <ol class="carousel-indicators">
    <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
    <li data-target="#carousel-example-generic" data-slide-to="1"></li>
	<li data-target="#carousel-example-generic" data-slide-to="2"></li>
    
  </ol>

  <!-- Wrapper for slides -->
  <div class="carousel-inner" role="listbox">
    <div class="item active">
      <img src="img/tilicho_lake.jpg" alt="Pokhara">
      <div class="carousel-caption">
       <h3>Gregor Samsa woke</h3>
	   <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit</p>
      </div>
    </div>
    <div class="item">
      <img src="img/boudhanath.jpg" alt="Boudha">
      <div class="carousel-caption">
       <h3>Gregor Samsa woke</h3>
	   <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit</p>
      </div>
    </div>
   <div class="item">
      <img src="img/pokhara.jpg" alt="Boudha">
      <div class="carousel-caption">
       <h3>Gregor Samsa woke</h3>
	   <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit</p>
      </div>
    </div>
  </div>

  <!-- Controls -->
  <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>
</div>
<div class="main">
  <div class="container">
    <div class="row">
      <div class="col-md-9">
        <div class="related-deals">
          <h2>Trekking <u><b>Packages</b></U> </h2>
          <div class="isotope-grid row">
            <div class="col-lg-4 col-sm-6 grid-item Panchase">
              <div class=" deal-post hotel">
                <div class="img-holder"> <a href="#"><img src="img/makalu-view-from-the-chhukhung-ri_640_480.jpg" alt="img description" class="img-responsive"></a> 
                <div class="btn-wrapper"><a href="#" class="btn">View Detail</a></div>
                </div>
                <div class="deal-description"> <a href="#"><strong>Panchase Treks (3-5Days) </strong></a>
                  <p>Panchase trek can be extended in several ways From Panchase it possible to trek through large Gurung village. </p>
                </div>
              </div>
            </div>
            <div class="col-lg-4 col-sm-6 grid-item Dhampus">
              <div class="deal-post flight">
                <div class="img-holder"> <a href="#"><img src="img/jomsom-muktinath-trek1469171412.jpg" alt="img description" class="img-responsive"></a> 
                <div class="btn-wrapper"><a href="#" class="btn">View Detail</a></div>
                </div>
                <div class="deal-description"> <a href="#"><strong>Dhampus Treks (2-3 Days)</strong></a>
                  <p>This is a fantastic two days trek that starts from Phedi which is at 45 mins driving distance from Pokhara.</p>
                </div>
              </div>
            </div>
            <div class="col-lg-4 col-sm-6 grid-item Pokhara Eco">
              <div class="deal-post activities">
                <div class="img-holder"> <a href="#"><img src="img/d51.jpg" alt="img description" class="img-responsive"></a> <a href="#"></a> 
                <div class="btn-wrapper"><a href="#" class="btn">View Detail</a></div>
                </div>
                <div class="deal-description"> <a href="#"><strong>Pokhara Eco Treks (5-7 Days)</strong></a>
                  <p>This is a fantastic two days trek that starts from Phedi which is at  driving distance from Pokhara. </p>
                </div>
              </div>
            </div>
            <div class="col-lg-4 col-sm-6 grid-item Ghorepani">
              <div class="deal-post activities">
                <div class="img-holder"> <a href="#"><img src="img/d51.jpg" alt="img description" class="img-responsive"></a> <a href="#"></a> 
                <div class="btn-wrapper"><a href="#" class="btn">View Detail</a></div>
                </div>
                <div class="deal-description"> <a href="#"><strong>Ghorepani Poon Hill Treks (3-5 Days)</strong></a>
                  <p>Ghorepani Ghandruk trek is fairly easy and colorful one into the Annapurna region. Throughout the Ghorepani Ghandruk
                    trek can enjoy spectacular mountains</p>
                </div>
              </div>
            </div>
            <div class="col-lg-4 col-sm-6 grid-item Mardi Himal">
              <div class="deal-post flight">
                <div class="img-holder"> <a href="#"><img src="img/makalu-view-from-the-chhukhung-ri_640_480.jpg" alt="img description" class="img-responsive"></a> 
                <div class="btn-wrapper"><a href="#" class="btn">View Detail</a></div>
                </div>
                <div class="deal-description"> <a href="#"><strong>Mardi Himal Treks (5-7 Days)</strong></a>
                  <p>The Mardi Himal Trek is a newly opened route and hidden treasure in the Annapurna region. This trek is best destination to avoid the crowd. Mardi Himal trek offer imaging view of Annapurna, Dhaulagiri.</p>
                </div>
              </div>
            </div>
            <div class="col-lg-4 col-sm-6 grid-item Annapurna">
              <div class="deal-post hotel">
                <div class="img-holder"> <a href="#"><img src="img/jomsom-muktinath-trek1469171412.jpg" alt="img description" class="img-responsive"></a> 
                <div class="btn-wrapper"><a href="#" class="btn">View Detail</a></div>
                </div>
                <div class="deal-description"> <a href="#"><strong>Annapurna Base Camp Treks (7-10 Days) </strong></a>
                  <p>Annapurna Base Camp Trek is known as Annapurna Sanctuary.Trekking starts from either Nayapul or Dhampus Phedi, there are 2 differences option in doing Annapurna Base Camp.</p>
                </div>
              </div>
            </div>
            <div class="col-lg-4 col-sm-6 grid-item Jomsom">
              <div class=" deal-post hotel">
                <div class="img-holder"> <a href="#"><img src="img/makalu-view-from-the-chhukhung-ri_640_480.jpg" alt="img description" class="img-responsive"></a> 
                <div class="btn-wrapper"><a href="#" class="btn">View Detail</a></div>
                </div>
                <div class="deal-description"> <a href="#"><strong>Jomsom Muktinath Treks (7-9 Days) </strong></a>
                  <p>The Jomsom Muktinath Trek is  adventure to high mountain region of western Nepal dominated by Dhaulagiri & Nilgiri Himalaya Range.</p>
                </div>
              </div>
            </div>
            <div class="col-lg-4 col-sm-6 grid-item Tilicho ">
              <div class="deal-post flight">
                <div class="img-holder"> <a href="#"><img src="img/jomsom-muktinath-trek1469171412.jpg" alt="img description" class="img-responsive"></a> 
                <div class="btn-wrapper"><a href="#" class="btn">View Detail</a></div>
                </div>
                <div class="deal-description"> <a href="#"><strong>Tilicho Lake Treks (8-12 Days) </strong></a>
                  <p>Tilicho lake short trekking is the adventure trek to explore the difficult trekking trail as well as the beauty of the Tilicho Lake.</p>
                </div>
              </div>
            </div>
            <div class="col-lg-4 col-sm-6 grid-item Khopra">
              <div class="deal-post activities">
                <div class="img-holder"> <a href="#"><img src="img/d51.jpg" alt="img description" class="img-responsive"></a> <a href="#"></a> 
                <div class="btn-wrapper"><a href="#" class="btn">View Detail</a></div>
                </div>
                <div class="deal-description"> <a href="#"><strong>Khopra (Khayar Lake) Treks (5-7 Days) </strong></a>
                  <p>Khopra Khayar Lake Trek is Himalayan magnificent and easily access, up to 7 days walking itinerary. </p>
                </div>
              </div>
            </div>
            <div class="col-lg-4 col-sm-6 grid-item RoundAnnapurna">
              <div class="deal-post activities">
                <div class="img-holder"> <a href="#"><img src="img/d51.jpg" alt="img description" class="img-responsive"></a> <a href="#"></a> 
                <div class="btn-wrapper"><a href="#" class="btn">View Detail</a></div>
                </div>
                <div class="deal-description"> <a href="#"><strong>Round Annapurna Treks (12-15 Days)</strong></a>
                  <p>The Annapurna Circuit in Nepal is one of the worlds classic multiday hikes. While the start & stop points & route can be variable, in general trekkers go from Besisahar to Nayapul, roughly 134 miles.</p>
                </div>
              </div>
            </div>
            <div class="col-lg-4 col-sm-6 grid-item Langtang">
              <div class="deal-post flight">
                <div class="img-holder"> <a href="#"><img src="img/makalu-view-from-the-chhukhung-ri_640_480.jpg" alt="img description" class="img-responsive"></a> 
                <div class="btn-wrapper"><a href="#" class="btn">View Detail</a></div>
                </div>
                <div class="deal-description"> <a href="#"><strong>Langtang Treks (7-10 Days)</strong></a>
                  <p>Langtang valley trekking is also called the valley of glaciers trek. Langtang is the closet place where you could get the massive snow capped mountains and beautiful glaciers. This valley is located only 19 miles north of Kathmandu. </p>
                </div>
              </div>
            </div>
            <div class="col-lg-4 col-sm-6 grid-item EverestBaseCamp">
              <div class="deal-post hotel">
                <div class="img-holder"> <a href="#"><img src="img/jomsom-muktinath-trek1469171412.jpg" alt="img description" class="img-responsive"></a> 
                <div class="btn-wrapper"><a href="#" class="btn">View Detail</a></div>
                </div>
                <div class="deal-description"> <a href="#"><strong>Mt. Everest Base Camp Treks (10-18 Days)  </strong></a>
                  <p>Trekking to the Everest Base Camp also provides us an opportunity to embark on an epic journey that Sir Edmund Hillary and Tenzing Norgay set off to in 1953.</p>
                </div>
              </div>
            </div>
            <div class="col-lg-4 col-sm-6 grid-item RoundManaslu">
              <div class=" deal-post hotel">
                <div class="img-holder"> <a href="#"><img src="img/makalu-view-from-the-chhukhung-ri_640_480.jpg" alt="img description" class="img-responsive"></a> 
                <div class="btn-wrapper"><a href="#" class="btn">View Detail</a></div>
                </div>
                <div class="deal-description"> <a href="#"><strong>Round Manaslu Treks (10-15 Days) </strong></a>
                  <p>Manaslu trekking exhibits the sensational mountain vistas, fascinating culture  spanning altitudes from 600 to 5500 meter – one of the Nepal’s last Himalayan jewels.</p>
                </div>
              </div>
            </div>
            <div class="col-lg-4 col-sm-6 grid-item Dhaulagiri">
              <div class="deal-post flight">
                <div class="img-holder"> <a href="#"><img src="img/jomsom-muktinath-trek1469171412.jpg" alt="img description" class="img-responsive"></a> 
                <div class="btn-wrapper"><a href="#" class="btn">View Detail</a></div>
                </div>
                <div class="deal-description"> <a href="#"><strong>Dhaulagiri Ice Fall Treks (12-16 Days)</strong></a>
                  <p>Dhaulagiri, the seventh highest mountain in the world, is situated in Nepal and forms part of the Dhaulagiri mountain range.
                  Its name is derived from Sanskrit </p>
                </div>
              </div>
            </div>
            <div class="col-lg-4 col-sm-6 grid-item UpperMustang">
              <div class="deal-post activities">
                <div class="img-holder"> <a href="#"><img src="img/d51.jpg" alt="img description" class="img-responsive"></a> <a href="#"></a> 
                <div class="btn-wrapper"><a href="#" class="btn">View Detail</a></div>
                </div>
                <div class="deal-description"> <a href="#"><strong>Upper Mustang Treks (10-12 Days)</strong></a>
                  <p>Upper Mustang Trek to hidden paradise of Mustang is fabulous tour that visitors experience ever best holiday trip
              in their life in Nepal. Upper Mustang described as a mystique valley </p>
                </div>
              </div>
            </div>
            
          </div>
          <a href="#" class="btn view-btn">View More</a> </div>
        <div class="related-deals">
          <h2>Tour <a href="#">Packages</a> </h2>
          <div class="isotope-container row">
            <div class="col-lg-4 col-sm-6 isotope-item makalu">
              <div class=" deal-post hotel">
                <div class="img-holder"> <a href="#"><img src="img/makalu-view-from-the-chhukhung-ri_640_480.jpg" alt="img description" class="img-responsive"></a> 
                <div class="btn-wrapper"><a href="#" class="btn">View Detail</a></div>
                </div>
                <div class="deal-description"> <a href="#"><strong>Makalu Trek - 22 Days</strong></a>
                  <p>Makalu trekking in eastern Nepal is protected by the Makalu Barun National Park and Conservation Project. </p>
                </div>
              </div>
            </div>
            <div class="col-lg-4 col-sm-6 isotope-item jomsom">
              <div class="deal-post flight">
                <div class="img-holder"> <a href="#"><img src="img/jomsom-muktinath-trek1469171412.jpg" alt="img description" class="img-responsive"></a> 
                <div class="btn-wrapper"><a href="#" class="btn">View Detail</a></div>
                </div>
                <div class="deal-description"> <a href="#"><strong>Jomsom Trek - 13 Days</strong></a>
                  <p>An exhilarating trek in the Annapurna and Mustang regions of Nepal, complete with diverse landscape</p>
                </div>
              </div>
            </div>
            <div class="col-lg-4 col-sm-6 isotope-item dolpa">
              <div class="deal-post activities">
                <div class="img-holder"> <a href="#"><img src="img/d51.jpg" alt="img description" class="img-responsive"></a> <a href="#"></a><div class="btn-wrapper"><a href="#" class="btn">View Detail</a></div> 
                </div>
                <div class="deal-description"> <a href="#"><strong>Dolpo Trek - 26 Days</strong></a>
                  <p>The Dolpo resembles the mysterious and spiritual aura in the hidden valleys of western Nepal.</p>
                </div>
              </div>
            </div>
            <div class="col-lg-4 col-sm-6 isotope-item makalu">
              <div class=" deal-post hotel">
                <div class="img-holder"> <a href="#"><img src="img/makalu-view-from-the-chhukhung-ri_640_480.jpg" alt="img description" class="img-responsive"></a> 
                <div class="btn-wrapper"><a href="#" class="btn">View Detail</a></div>
                </div>
                <div class="deal-description"> <a href="#"><strong>Makalu Trek - 22 Days</strong></a>
                  <p>Makalu trekking in eastern Nepal is protected by the Makalu Barun National Park and Conservation Project. </p>
                </div>
              </div>
            </div>
            <div class="col-lg-4 col-sm-6 isotope-item jomsom">
              <div class="deal-post flight">
                <div class="img-holder"> <a href="#"><img src="img/jomsom-muktinath-trek1469171412.jpg" alt="img description" class="img-responsive"></a> 
                <div class="btn-wrapper"><a href="#" class="btn">View Detail</a></div>
                </div>
                <div class="deal-description"> <a href="#"><strong>Jomsom Trek - 13 Days</strong></a>
                  <p>An exhilarating trek in the Annapurna and Mustang regions of Nepal, complete with diverse landscape</p>
                </div>
              </div>
            </div>
            <div class="col-lg-4 col-sm-6 isotope-item dolpa">
              <div class="deal-post activities">
                <div class="img-holder"> <a href="#"><img src="img/d51.jpg" alt="img description" class="img-responsive"></a> <a href="#"></a> 
                <div class="btn-wrapper"><a href="#" class="btn">View Detail</a></div>
                </div>
                <div class="deal-description"> <a href="#"><strong>Dolpo Trek - 26 Days</strong></a>
                  <p>The Dolpo resembles the mysterious and spiritual aura in the hidden valleys of western Nepal.</p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-md-3">
        <div class="adsbanner mt100"> <a href="#"><img src="img/dunhill300x250a-1.gif" alt=""></a> </div>
        <div class="adsbanner"><a href="#"><img src="img/adsbanner1.jpg" alt=""></a></div>
        <div class="adsbanner"><a href="#"><img src="img/adsbanner2.jpg" alt=""></a></div>
      </div>
    </div>
    
    <div class="related-deals extradiv">
    <h2><a href="#">Facilities</a> </h2>
      <div class="row">
        <div class="col-md-3 col-sm-3">
          <div class="image-box"> <img src="img/13909274.jpg" alt="">
            <div class="overlay">
              <h4><a href="#">Hotel Reservation</a></h4>
            </div>
          </div>
        </div>
        <div class="col-md-3 col-sm-3">
          <div class="image-box"> <img src="img/_DSC7142.JPG" alt="">
            <div class="overlay">
              <h4><a href="#">Air Ticketing</a></h4>
            </div>
          </div>
        </div>
        <div class="col-md-3 col-sm-3">
          <div class="image-box"> <img src="img/blog.jpg" alt="">
            <div class="overlay">
              <h4><a href="#">Bus & Train Tickets</a></h4>
            </div>
          </div>
        </div>
        <div class="col-md-3 col-sm-3">
          <div class="image-box"> <img src="img/blog.jpg" alt="">
            <div class="overlay">
              <h4><a href="#">Vehicles On Hire</a></h4>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="testimonial-wrapper related-deals text-center">
      <h2>Our Happy<a href="#">Clients</a> </h2>
      <div class="owl-carousel content-slider">
        <div class="testimonial clearfix">
          <div class="testimonial-body">
            <p></i>Sed ut perspiciatis unde omnis iste natu error sit voluptatem accusan tium dolore laud antium,  totam rem dolor sit amet tristique pulvinar, turpis arcu rutrum nunc, ac laoreet turpis augue a justo.</p>
            <div class="testimonial-info-1">- Jane Doe</div>
            <div class="testimonial-info-2">By Company</div>
          </div>
        </div>
        <div class="testimonial clearfix">
          <div class="testimonial-body">
            <p>Sed ut perspiciatis unde omnis iste natu error sit voluptatem accusan tium dolore laud antium,  totam rem dolor sit amet tristique pulvinar, turpis arcu rutrum nunc, ac laoreet turpis augue a justo.</p>
            <div class="testimonial-info-1">- Jane Doe</div>
            <div class="testimonial-info-2">By Company</div>
          </div>
        </div>
        <div class="testimonial clearfix">
          <div class="testimonial-body">
            <p>Sed ut perspiciatis unde omnis iste natu error sit voluptatem accusan tium dolore laud antium,  totam rem dolor sit amet tristique pulvinar, turpis arcu rutrum nunc, ac laoreet turpis augue a justo.</p>
            <div class="testimonial-info-1">- Jane Doe</div>
            <div class="testimonial-info-2">By Company</div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<?php include('footer.php')?>
