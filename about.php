﻿<?php include('header.php')?>
<div class="pagetop">
	<div class="container">
    	<div class="row">
        	<div class="col-md-6 col-sm-6">
            	<h2>About Us</h2>
            </div>
            <div class="col-md-6 col-sm-6">
            	<ol class="breadcrumb pull-right">
  <li><a href="index.php">Home</a></li>
  <li class="active">About Us</li>
</ol>
            </div>
        </div>
    </div>
</div>

	<div class="main">
		<div class="container">
        <div class="row">
        	<div class="col-md-6 col-sm-6">
            	<div class="content-box">
                	<div class="page-title">
                    	<h3>Introduction</h3>
                    </div>
                    <p>Mountain House Treks & Expedition is registered as per the Company Act of Nepal after the approval of Nepal Tourism Board NTB with a aim to conduct the outbound tourism and to devolve a part of the revenue thus generated to build the infrastructure for Home stay safe and ultimately rising the living standard of Nepalese People. We aim towards bringing people together and knowing each other. Hamro Nepal is on your service for your every need about package or in individual by our employees using experts and natives for content creation for Expedition, Adventure , Nature exploration, Honeymoon, Botanical study, Film making and Photography. Our valued customers are always given the topmost priority by the team of our company with great hospitality. Customer safety satisfaction and their value for money is our upmost priority along with great pleasure and hasslefree service. Ultimately flourishing the tourism Industry.</p>
                    <div class="page-title">
                    	
                        <h3>Our Mission</h3>
                    </div>
                    <p>
                    	Leader in Nepalese Tourism Industry in terms of Ticketing, Tour Expedition by attending customer satisfaction and priority at the upmost degree along with the growing the company infrastructures coverage and contributing community for sustainable livelihood, through providing life enriching experiences. Our mission is to brand our self as provider of the ultimate travel planning experience while becoming a one stop for every travel services available in the industry
                    </p>
                     <div class="page-title">
                    	
                        <h3>Our Vision</h3>
                    </div>
                    <p>
                    	Each and every one of us working on HNTT live and breathe for researching organizing ,managing and negotiation the best value deals and pass on it to you so that every travelers is guaranteed to receive maximum value and services
                    </p>
                </div>
            </div>
            <div class="col-md-6 col-sm-6">
            <div class="primary-img"><img src="img/Our_Mission.png" alt=""></div>
            	<div class="primary-img"><img src="img/img.png" alt=""></div>
                
            </div>
        </div>
        <blockquote>
        	 <div class="page-title mt0">
                    	
                        <h3>The Idea Message</h3>
                    </div>
                    Nima Sherpa owned a lodge in Kutumsang in central Nepal when the earthquake of April 2015 struck and levelled every building in his village. Since then, he has had to build temporary wooden structures to house his family and to shelter trekkers.Â The worst natural disaster to hit Nepal since 1934, the earthquake killed over 8,000 people, injured more than 21,000, and left two million homeless, with entire villages flattened across many districts of the country. A year on from the earthquake, blend of professional experts from the sector of Aviation ,Hospitality and allied tourism business decided to support this magnificent and bustling country of Nepal and its people to stand proudly on her feet once again. We are determined in our effort to empower local communities and helping these communities reset their course on the path to prosperity as we devolve 5 of our sales to build the infrastructure to make Home stay safe.
        </blockquote>
		</div>
	</div>
<?php include('footer.php')?>    