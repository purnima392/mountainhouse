<?php include('header.php')?>
<!--Title Bar Start-->
<div class="title-bar interfece clearfix">
	<div class="container">
		<div class="row">
			<div class="col-md-4 col-sm-4 col-xs-12">
				<div class="left-side">
					<div class="palcename">
						<h2>Thailand</h2>
					</div>
					<div class="icon-tag pull-right"><img src="img/icon1.jpg" alt="" > </div>
				</div>
			</div>
			<div class="col-md-8 col-sm-8 col-xs-12">
				<div class="right-side">
					<div class="side-text">
						<p>Description text of the city, or region or country. Description text of <br>
							the city, or region or country. Description text of the city, or region or <br>
							country. Description text of the city, or region or country. </p>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!--Title Bar End-->
<div class="trip-wrapper clearfix">
	<div class="container">
		<div class="row">
			<div class="col-md-4 col-sm-4 col-xs-12">
				<div class="left-sidebar">
					<div class="date-wrap">
						<h3>Filter results</h3>
						<div class="date-form">
							<label>Dates:</label>
							<input type="text" class="datepicker">
							<span>to</span>
							<input type="text" class="datepicker">
							<div class="service-list">
								<p>Looking for:<span>(click items to select)</span></p>
								<ul class="clearfix">
									<li><a href="#">Resturants</a><span>98</span></li>
									<li><a href="#">Hotels</a><span>75</span></li>
									<li><a href="#">Flights</a><span>43</span></li>
									<li class="grey-btn"><a href="#">Packages</a><span>41</span></li>
									<li class="grey-btn"><a href="#">Health & Spas</a><span>43</span></li>
									<li class="grey-btn"><a href="#">Activities</a><span>5</span></li>
								</ul>
							</div>
							<div class="service-list">
								<p>Cities:<span>(click items to select)</span></p>
								<ul class="clearfix">
									<li><a href="#">Resturants</a><span>98</span></li>
									<li><a href="#">Hotels</a><span>75</span></li>
									<li><a href="#">Flights</a><span>43</span></li>
									<li class="grey-btn"><a href="#">Packages</a><span>41</span></li>
									<li class="grey-btn"><a href="#">Health & Spas</a><span>43</span></li>
									<li class="grey-btn"><a href="#">Activities</a><span>5</span></li>
								</ul>
								<!-- Indicates a successful or positive action -->
								<button type="button" class="btn btn-success">APPLY CHANGES</button>
							</div>
						</div>
					</div>
					<div class="left-bottom text-center">
						<h2>Share Your Search</h2>
						<ul class="social-share">
							<li class="twitter1"><a href="#">twitter</a></li>
							<li class="facebook1"><a href="#">Facebook</a></li>
							<li class="gpluse1"><a href="#">Gpluse</a></li>
							<li class="linkedin1"><a href="#">Linked in</a></li>
							<li class="pintrest1"><a href="#">Pintrest</a></li>
						</ul>
						<h2>Receive the best deals by email</h2>
						<form id="newsearch" method="get" action="#">
							<input type="text" class="textinput" name="q" size="21" maxlength="120">
							<input type="submit" value="OK" class="search-button">
						</form>
					</div>
				</div>
			</div>
			<div class="col-md-8 col-sm-8 col-xs-12">
				<div class="right-side-content">
					<div class="row">
						<div class="col-md-6 col-sm-12 col-xs-12">
							<div class="deal-post flight">
								<div class="img-holder"> <a href="#"><img class="img-responsive" alt="img description" src="img/img3.jpg"></a> <a href="#"><span class="flight-tag">Flights</span></a> </div>
								<div class="deal-description"> <span class="place">Sydney, Australia</span>
									<div class="row">
										<div class="col-lg-7 col-md-6 col-sm-6 col-xs-6"> <strong>Sydney &gt; Singapore</strong> </div>
										<div class="col-lg-5 col-md-6 col-sm-6 col-xs-6 pull-right">
											<div class="cost"> <span class="discount">-50%</span> $350 </div>
										</div>
									</div>
									<p>Escape at the best prices!<img class="pull-right" alt="" src="img/flag2.png"></p>
								</div>
							</div>
						</div>
						<div class="col-md-6 col-sm-12 col-xs-12">
							<div class="deal-post activities">
								<div class="img-holder"> <a href="#"><img class="img-responsive" alt="img description" src="img/img4.jpg"></a> <a href="#"><span class="activities-tag">Activities</span></a> </div>
								<div class="deal-description"> <span class="place">Ho Chi Minh, Vietnam</span>
									<div class="row">
										<div class="col-lg-7 col-md-6 col-sm-6 col-xs-6"> <strong>Mekong River Day Cruise</strong> </div>
										<div class="col-lg-5 col-md-6 col-sm-6 col-xs-6 pull-right">
											<div class="cost"> <span class="discount">-70%</span> $22 </div>
										</div>
									</div>
									<p>A timeless wonder of Vietnam!<img class="pull-right" alt="" src="img/flag1.png"></p>
								</div>
							</div>
						</div>
						<div class="col-md-6 col-sm-12 col-xs-12">
							<div class="deal-post flight">
								<div class="img-holder"> <a href="#"><img class="img-responsive" alt="img description" src="img/img3.jpg"></a> <a href="#"><span class="flight-tag">Flights</span></a> </div>
								<div class="deal-description"> <span class="place">Sydney, Australia</span>
									<div class="row">
										<div class="col-lg-7 col-md-6 col-sm-6 col-xs-6"> <strong>Sydney &gt; Singapore</strong> </div>
										<div class="col-lg-5 col-md-6 col-sm-6 col-xs-6 pull-right">
											<div class="cost"> <span class="discount">-50%</span> $350 </div>
										</div>
									</div>
									<p>Escape at the best prices!<img class="pull-right" alt="" src="img/flag2.png"></p>
								</div>
							</div>
						</div>
						<div class="col-md-6 col-sm-12 col-xs-12">
							<div class="deal-post activities">
								<div class="img-holder"> <a href="#"><img class="img-responsive" alt="img description" src="img/img4.jpg"></a> <a href="#"><span class="activities-tag">Activities</span></a> </div>
								<div class="deal-description"> <span class="place">Ho Chi Minh, Vietnam</span>
									<div class="row">
										<div class="col-lg-7 col-md-6 col-sm-6 col-xs-6"> <strong>Mekong River Day Cruise</strong> </div>
										<div class="col-lg-5 col-md-6 col-sm-6 col-xs-6 pull-right">
											<div class="cost"> <span class="discount">-70%</span> $22 </div>
										</div>
									</div>
									<p>A timeless wonder of Vietnam!<img class="pull-right" alt="" src="img/flag1.png"></p>
								</div>
							</div>
						</div>
						<div class="col-md-6 col-sm-12 col-xs-12">
							<div class="deal-post flight">
								<div class="img-holder"> <a href="#"><img class="img-responsive" alt="img description" src="img/img3.jpg"></a> <a href="#"><span class="flight-tag">Flights</span></a> </div>
								<div class="deal-description"> <span class="place">Sydney, Australia</span>
									<div class="row">
										<div class="col-lg-7 col-md-6 col-sm-6 col-xs-6"> <strong>Sydney &gt; Singapore</strong> </div>
										<div class="col-lg-5 col-md-6 col-sm-6 col-xs-6 pull-right">
											<div class="cost"> <span class="discount">-50%</span> $350 </div>
										</div>
									</div>
									<p>Escape at the best prices!<img class="pull-right" alt="" src="img/flag2.png"></p>
								</div>
							</div>
						</div>
						<div class="col-md-6 col-sm-12 col-xs-12">
							<div class="deal-post activities">
								<div class="img-holder"> <a href="#"><img class="img-responsive" alt="img description" src="img/img4.jpg"></a> <a href="#"><span class="activities-tag">Activities</span></a> </div>
								<div class="deal-description"> <span class="place">Ho Chi Minh, Vietnam</span>
									<div class="row">
										<div class="col-lg-7 col-md-6 col-sm-6 col-xs-6"> <strong>Mekong River Day Cruise</strong> </div>
										<div class="col-lg-5 col-md-6 col-sm-6 col-xs-6 pull-right">
											<div class="cost"> <span class="discount">-70%</span> $22 </div>
										</div>
									</div>
									<p>A timeless wonder of Vietnam!<img class="pull-right" alt="" src="img/flag1.png"></p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php include('footer.php')?>    